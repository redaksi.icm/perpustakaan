<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu" style="color: white">
            <li style="color: white">
                <a href="<?= base_url().'web'; ?>" class="waves-effect waves-dark"><i class="fa fa-fw fa-home"></i>Beranda</a>
            </li>

            <?php if($this->session->userdata('role') == 1){ ?>
            <li style="color: white">
                <a href="<?= base_url().'buku/admin_management'; ?>" class="waves-effect waves-dark"><i class="fa fa-fw fa-edit"></i>Input Buku</a>
            </li>
            <li style="color: white">
                <a href="<?= base_url().'buku/admin_daftar'; ?>" class="waves-effect waves-dark"><i class="fa fa-fw fa-list"></i>Data Management Buku</a>
            </li>
            <?php } ?>
            <li style="color: white">
                <a href="<?= base_url().'buku/cari'; ?>" class="waves-effect waves-dark"><i class="fa fa-fw fa-search"></i>Pencarian Buku</a>
            </li>
        </ul>

    </div>

</nav>