<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
	
		<div class="card horizontal cardIcon waves-effect waves-dark">
		<div class="card-image blue">
		<i class="material-icons dp48">books</i>
		</div>
		<div class="card-stacked blue">
		<div class="card-content">
		<h3 style="color: white"><?= $jumlah_buku; ?></h3> 
		</div>
		<div class="card-action">
		<strong style="color: white">Total Buku</strong>
		</div>
		</div>
		</div>

    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
	
		<div class="card horizontal cardIcon waves-effect waves-dark">
		<div class="card-image green">
		<i class="material-icons dp48">account_circle</i>
		</div>
		<div class="card-stacked green">
		<div class="card-content">
		<h3 style="color: white"><?= $jumlah_anggota; ?></h3> 
		</div>
		<div class="card-action">
		<strong style="color: white">Total Anggota</strong>
		</div>
		</div>
		</div> 
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
	
		<div class="card horizontal cardIcon waves-effect waves-dark">
		<div class="card-image red">
		<i class="material-icons dp48">books</i>
		</div>
		<div class="card-stacked red">
		<div class="card-content">
		<h3><?= $jumlah_buku_seminggu; ?></h3> 
		</div>
		<div class="card-action">
		<strong>Jumlah Pertambahan Buku dalam Satu Mingu</strong>
		</div>
		</div>
		</div>

    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
	
		<div class="card horizontal cardIcon waves-effect waves-dark">
		<div class="card-image orange">
		<i class="material-icons dp48">account_circle</i>
		</div>
		<div class="card-stacked orange">
		<div class="card-content">
		<h3 style="color: white"><?= $jumlah_anggota_seminggu; ?></h3> 
		</div>
		<div class="card-action">
		<strong style="color: white">Jumlah Pertambahan Anggota dalam Satu Minggu</strong>
		</div>
		</div>
		</div> 
    </div>
</div>

<div class="row" style="background-color: white">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<center>
			<img src="<?= base_url().'/assets/gambar/dashboard.png'?>" style="width: 70%">
		</center>
    </div>                   
</div>