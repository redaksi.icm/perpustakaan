 <div class="row">
			 <div class="col-lg-12">
			 <div class="card">
        <div class="card-action">
            Edit Data Buku
        </div>
        <div class="card-content">
    <form method="POST" enctype="multipart/form-data" class="col s12" >
      <div class="row">
        <div class="col s12">
          <label>Judul Buku</label>
          <input name="id" type="hidden" value="<?= $data_buku->id; ?>">
          <input id="judul_buku" name="judul" type="text" value="<?= $data_buku->judul; ?>">
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <label for="text">Penulis</label>
          <input id="penulis" name="penulis" type="text" class="validate" value="<?= $data_buku->penulis; ?>">
        </div>
      </div>
      <div class="form-group">
        <label for="sel1">Kategori Buku:</label>
        <select class="form-control" id="sel1" name="kategori_buku">
          <option value="Penyu" <?php if($data_buku->kategori_buku == 'Penyu'){ echo "selected";} ?>>Penyu</option>
          <option value="Terumbu Karang" <?php if($data_buku->kategori_buku == 'Terumbu Karang'){ echo "selected";} ?>>Terumbu Karang</option>
          <option value="Lamun" <?php if($data_buku->kategori_buku == 'Lamun'){ echo "selected";} ?>>Lamun</option>
          <option value="Mangrove" <?php if($data_buku->kategori_buku == 'Mangrove'){ echo "selected";} ?>>Mangrove</option>
          <option value="Mollusca" <?php if($data_buku->kategori_buku == 'Mollusca'){ echo "selected";} ?>>Mollusca</option>
          <option value="Elang" <?php if($data_buku->kategori_buku == 'Elang'){ echo "selected";} ?>>Elang</option>
          <option value="Mamalia Laut" <?php if($data_buku->kategori_buku == 'Mamalia Laut'){ echo "selected";} ?>>Mamalia Laut</option>
          <option value="Ikan Karang" <?php if($data_buku->kategori_buku == 'Ikan Karang'){ echo "selected";} ?>>Ikan Karang</option>
          <option value="Hutan Pantai" <?php if($data_buku->kategori_buku == 'Hutam Pantai'){ echo "selected";} ?>>Hutan Pantai</option>
          <option value="Ekowisata" <?php if($data_buku->kategori_buku == 'Ekowisata'){ echo "selected";} ?>>Ekowisata</option>
          <option value="Sosial" <?php if($data_buku->kategori_buku == 'Sosial'){ echo "selected";} ?>>Sosial</option>
          <option value="Manajemen Pengelolaan" <?php if($data_buku->kategori_buku == 'Manajemen Pengelolaan'){ echo "selected";} ?>>Manajemen Pengelolaan</option>
          <option value="Umum" <?php if($data_buku->kategori_buku == 'Umum'){ echo "selected";} ?>>Umum</option>
        </select>
      </div>
      <div class="form-group">
        <label for="sel1">Jenis Buku:</label>
        <select class="form-control" id="sel1" name="jenis_buku">
          <option value="Skripsi" <?php if($data_buku->kategori_buku == 'Skripsi'){ echo "selected";} ?>>Skripsi</option>
          <option value="Tesis" <?php if($data_buku->kategori_buku == 'Tesis'){ echo "selected";} ?>>Tesis</option>
          <option value="Disertasi" <?php if($data_buku->kategori_buku == 'Disertasi'){ echo "selected";} ?>>Disertasi</option>
          <option value="Laporan" <?php if($data_buku->kategori_buku == 'Laporan'){ echo "selected";} ?>>Laporan</option>
          <option value="Lain-lain" <?php if($data_buku->kategori_buku == 'Lain-lain'){ echo "selected";} ?>>Lain-lain</option>
        </select>
      </div>
      <div class="row">
        <div class="col s12">
          <label for="text">Tahun Terbit</label>
          <input id="thn_terbit" name="thn_terbit" type="text" class="validate" value="<?= $data_buku->thn_terbit; ?>">
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <label for="text">Instansi</label>
          <input id="instansi" name="instansi" type="text" class="validate" value="<?= $data_buku->instansi; ?>">
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <label for="text">Keterangan</label>
          <input id="jenis_buku" name="keterangan" type="text" class="validate" value="<?= $data_buku->keterangan; ?>">
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <label for="text">File: 
            <?php if($data_buku->upload != ""){ ?>
              <a href="<?= base_url().'upload_buku/'.$data_buku->upload; ?>">Download File Sebelumnya</a>
              <input name="upload_url" type="text" value="<?= $data_buku->upload; ?>">
            <?php } ?>
          </label>
          <input id="upload_buku" name="upload_buku" type="file" class="validate">
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <button type="submit" name="submit" value="submit" class="btn btn-danger">Update Data Buku</button>
        </div>
      </div>
    </form>
	<div class="clearBoth"></div>
  </div>
    </div>
 </div>	
	 </div>		