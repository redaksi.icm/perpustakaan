<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->
        <div class="card">
            <div class="card-action">
            </div>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th class="center">Judul Buku</th>
                                <th class="center">Penulis</th>
                                <th class="center">Tahun Terbit</th>
                                <th class="center">Jenis Buku</th>
                                <th class="center">Kategori Buku</th>
                                <th class="center">Instansi</th>
                                <th class="center">Keterangan</th>
                                <th class="center">File</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach ($daftar_buku as  $buku) :?>
                            <tr class="odd gradeA">
                               <td><?= $no ?></td>
                                <td><?= $buku->judul ?></td>
                                <td><?= $buku->penulis ?></td>
                                <td><?= $buku->thn_terbit ?></td>
                                <td><?= $buku->jenis_buku ?></td>
                                <td><?= $buku->kategori_buku ?></td>
                                <td><?= $buku->instansi ?></td>
                                <td><?= $buku->keterangan ?></td>
                                <td class="center">
                                    <?php if($buku->upload != ""){ ?>
                                        <a href="<?= base_url().'upload_buku/'.$buku->upload;?>">Lihat Buku</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php $no++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>
