 <div class="row">
			 <div class="col-lg-12">
			 <div class="card">
        <div class="card-action">
            Masukan Data Buku
        </div>
        <div class="card-content">
    <form method="POST" enctype="multipart/form-data" class="col s12" >
      <div class="row">
        <div class="input-field col s12">
          <input id="judul_buku" name="judul" type="text" class="validate">
          <label for="text">Judul Buku</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="penulis" name="penulis" type="text" class="validate">
          <label for="text">Penulis</label>
        </div>
      </div>
      <div class="form-group">
        <label for="sel1">Kategori Buku:</label>
        <select class="form-control" id="sel1" name="kategori_buku">
          <option value="Penyu">Penyu</option>
          <option value="Terumbu Karang">Terumbu Karang</option>
          <option value="Lamun">Lamun</option>
          <option value="Mangrove">Mangrove</option>
          <option value="Mollusca">Mollusca</option>
          <option value="Elang">Elang</option>
          <option value="Mamalia Laut">Mamalia Laut</option>
          <option value="Ikan Karang">Ikan Karang</option>
          <option value="Hutan Pantai">Hutan Pantai</option>
          <option value="Ekowisata">Ekowisata</option>
          <option value="Sosial">Sosial</option>
          <option value="Manajemen Pengelolaan">Manajemen Pengelolaan</option>
          <option value="Umum">Umum</option>
        </select>
      </div>
      <div class="form-group">
        <label for="sel1">Jenis Buku:</label>
        <select class="form-control" id="sel1" name="jenis_buku">
          <option value="Skripsi">Skripsi</option>
          <option value="Tesis">Tesis</option>
          <option value="Disertasi">Disertasi</option>
          <option value="Laporan">Laporan</option>
          <option value="Lain-lain">Lain-lain</option>
        </select>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="thn_terbit" name="thn_terbit" type="text" class="validate">
          <label for="text">Tahun Terbit</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="instansi" name="instansi" type="text" class="validate">
          <label for="text">Instansi</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="jenis_buku" name="keterangan" type="text" class="validate">
          <label for="text">Keterangan</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="upload_buku" name="upload_buku" type="file" class="validate">
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <button type="submit" name="submit" value="submit" class="btn btn-danger">Masukan Data Buku</button>
        </div>
      </div>
    </form>
	<div class="clearBoth"></div>
  </div>
    </div>
 </div>	
	 </div>		