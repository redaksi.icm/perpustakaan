<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    public function login(){
        if($this->session->userdata('logged_in')){
            redirect('web');
        }
        $data['title'] = 'Halaman Login';
        if(!$this->input->post('submit')){
            $this->load->view('home/login', $data);
        } else {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $user = $this->user_model->login($username);

            if(password_verify($password, $user->password)){
                $data = [
                    'user_id' => $user->id,
                    'username' => $user->username,
                    'role' => $user->role,
                    'logged_in' => true
                ];
                $this->session->set_userdata($data);
                $this->session->set_flashdata('success', 'You are now logged in');
                redirect('web');
            } else {
                $this->session->set_flashdata('danger', 'Login is invalid');
                redirect('users/login');
            }
            
        }
    }


    public function registrasi(){
        $data['title'] = 'Halaman Registrasi';
        if(!$this->input->post('submit')){
            $this->load->view('home/registrasi',$data);
        } else {
            if($this->input->post('password') != $this->input->post('ulangi_password')){
                $this->session->set_flashdata('danger', 'Gagal Melakukan Registrai');
                redirect('users/registrasi');
            }
            else{
                $options = [
                    'cost' => 12,
                ];
                $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
                $this->user_model->register($password);
                $this->session->set_flashdata('success', 'Berhasil Melakukan Registrai');
                redirect('users/login');
            }
            
        }
        
    }

    public function logout(){
        $this->session->unset_userdata(['user_id', 'username', 'logged_in']);
        $this->session->set_flashdata('success', 'You are now logged out');
        redirect('users/login');
    }
    private function check_login(){
        // check login
        if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
    }



}
