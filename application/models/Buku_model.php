<?php 
class Buku_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
    public function get_buku_id($id){
        $query = $this->db->get_where('buku', array('id'=>$id));
        return $query->row();
    }
}