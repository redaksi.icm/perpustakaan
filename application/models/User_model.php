<?php 
class User_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }

    public function register($password){
        $waktu = date("Y-m-d");
        $data = [
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'password' => $password,
            'waktu' => date("Y-m-d")
        ];

        return $this->db->insert('akses', $data);
    }

    public function login($username){
        
        return $this->db->get_where('akses', [ 'username' => $username])->row();
    }
}