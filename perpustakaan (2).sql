-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 25, 2020 at 01:52 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `akses`
--

DROP TABLE IF EXISTS `akses`;
CREATE TABLE IF NOT EXISTS `akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(56) NOT NULL,
  `username` varchar(56) NOT NULL,
  `email` varchar(56) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role` int(11) NOT NULL,
  `waktu` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akses`
--

INSERT INTO `akses` (`id`, `nama_lengkap`, `username`, `email`, `password`, `role`, `waktu`) VALUES
(1, 'administrator', 'administrator', 'administrator@web', '$2y$12$UKkaiWivWb.UUeI5gOz/Eu.Jmo/dcoSMeVYA8FJ5Gzua9sl.8D4Cm', 1, '2020-06-25'),
(2, 'anggota', 'anggota', 'anggota@web', '$2y$12$DdMu0wd/KaEXmpAlerTlo.q36zJ.p7jnCBcWdvNxen1soNh2p8X2W', 0, '2020-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

DROP TABLE IF EXISTS `buku`;
CREATE TABLE IF NOT EXISTS `buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(256) NOT NULL,
  `penulis` varchar(100) NOT NULL,
  `thn_terbit` varchar(4) NOT NULL,
  `jenis_buku` varchar(30) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `kategori_buku` varchar(56) NOT NULL,
  `keterangan` text NOT NULL,
  `upload` text NOT NULL,
  `waktu` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id`, `judul`, `penulis`, `thn_terbit`, `jenis_buku`, `instansi`, `kategori_buku`, `keterangan`, `upload`, `waktu`) VALUES
(1, 'Laskar Pelangi', 'Andrea Hirata', '2009', 'Skripsi', 'Bentang Pustaka', 'Umum', 'Keterangan', 'fbdb0e26a43bc2ec0713265cfcaf1992.pdf', '2020-06-25');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
